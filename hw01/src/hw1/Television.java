package hw1;

/**
 * Model of a television with different kinds of functions, changing volume and
 * channels.
 * 
 * @author Yijia Huang
 *
 */
public class Television {

	/**
	 * The increment of volume is permanent in this class .
	 */
	public static final double VOLUME_INCREMENT = 0.07;

	/**
	 * The maximum channel number.
	 */
	private int MaxChannelNum;

	/**
	 * The current channel.
	 */
	private int CurrentChannel;

	/**
	 * The previous channel.
	 */
	private int PreviousChannel;

	/**
	 * The current volume.
	 */
	private double CurrentVolume;

	/**
	 * Construct a television with the given channel maximum.
	 * 
	 * @param givenChannelMax
	 * 
	 *            the maximum number of channels in this television
	 */

	public Television(int givenChannelMax) {
		MaxChannelNum = givenChannelMax - 1;
		CurrentChannel = PreviousChannel;
		CurrentVolume = 0.5;
		PreviousChannel = 0;
	}

	/**
	 * Changes the channel down by 1, wrapping around to the maximum channel
	 * number when the current channel hit to the zero.
	 */
	public void channelDown() {
		PreviousChannel = CurrentChannel;
		CurrentChannel = (CurrentChannel + MaxChannelNum) % (MaxChannelNum + 1);
	}

	/**
	 * Changes the channel up by 1, wrapping around to zero when the current
	 * channel hit to the maximum.
	 */
	public void channelUp() {
		PreviousChannel = CurrentChannel;
		CurrentChannel = (CurrentChannel + 1) % (MaxChannelNum + 1);
	}

	/**
	 * Returns a string representing the current channel and volume.
	 * 
	 * @return a string representing the current channel and volume
	 */
	public String display() {
		return "Channel " + CurrentChannel + " Volume " + (int) Math.round(CurrentVolume * 100) + "%";
	}

	/**
	 * Returns the current channel.
	 * 
	 * @return the current channel
	 */
	public int getChannel() {
		return CurrentChannel;
	}

	/**
	 * Returns the current volume.
	 * 
	 * @return the current volume
	 */
	public double getVolume() {
		return CurrentVolume;
	}

	/**
	 * Sets the current channel to the most recent previous channel.
	 */
	public void goToPreviousChannel() {
		CurrentChannel = PreviousChannel;
	}

	/**
	 * Resets this Television so that its available channels are now from 0
	 * through givenMax - 1.
	 * 
	 * @param givenMax
	 *            the channel maximum intended to set
	 */
	public void resetChannelMax(int givenMax) {
		MaxChannelNum = givenMax - 1;
		CurrentChannel = Math.min(CurrentChannel, givenMax - 1);
		PreviousChannel = Math.min(PreviousChannel, givenMax - 1);
	}

	/**
	 * Resets this Television so that its available channels are now from 0
	 * through givenMax - 1.
	 * 
	 * @param channelNumber
	 *            the channel number intended to set
	 */
	public void setChannel(int channelNumber) {
		PreviousChannel = CurrentChannel;
		CurrentChannel = Math.min(Math.max(channelNumber, 0), MaxChannelNum);
	}

	/**
	 * Lowers the volume by VOLUME_INCREMENT until 0.0.
	 */
	public void volumeDown() {
		CurrentVolume = Math.max(CurrentVolume - VOLUME_INCREMENT, 0);
	}

	/**
	 * Raises the volume by VOLUME_INCREMENT until 1.0.
	 */
	public void volumeUp() {
		CurrentVolume = Math.min(CurrentVolume + VOLUME_INCREMENT, 1.0);

	}

}
