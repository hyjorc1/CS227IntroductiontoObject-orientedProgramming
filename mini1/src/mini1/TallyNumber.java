package mini1;

import java.util.Scanner;

public class TallyNumber {

	private String str;
	private int num;

	public TallyNumber(int givenValue) {
		if (givenValue < 0) {
			num = -1;
			str = "";
		} else {
			num = givenValue;
			normalize();
		}
	}

	public TallyNumber(java.lang.String givenString) {
		str = new String(givenString);
		Scanner in = new Scanner(str);
		if (str == null || str.equals("")) {
			num = -1;
			str = "";
		} else {
			for (int a = 0; a < str.length(); a++) {
				if (str.charAt(a) != ' ' && str.charAt(a) != '*' && str.charAt(a) != '|' && str.charAt(a) != '0') {
					num = -1;
					str = "";
				} else {
					while (in.hasNext()) {
						String s = in.next();
						int temp = 0;
						for (int i = 0; i < s.length(); i++) {
							if (s.charAt(i) == '|')
								temp += 1;
							if (s.charAt(i) == '*')
								temp += 5;
						}
						num = 10 * num + temp;
					}
				}
			}
		}
		in.close();
	}

	public void add(TallyNumber other) {
		num += other.getIntValue();
		normalize();
	}

	public void combine(TallyNumber other) {
		num += other.getIntValue();
		String s = "";
		Scanner a = new Scanner(reverse(str));
		Scanner b = new Scanner(reverse(other.getStringValue()));
		while (b.hasNext() || a.hasNext()) {
			if (b.hasNext())
				s += b.next();
			if (a.hasNext())
				s += a.next();
			s += ' ';
		}
		str = new String(reverse(s.substring(0, s.length() - 1)));
		a.close();
		b.close();
	}

	public int getIntValue() {
		return num;
	}

	public java.lang.String getStringValue() {
		return str;
	}

	public void normalize() {
		int temp = num;
		str = "";
		for (int i = 0; i < Integer.toString(num).length(); i++) {
			for (int k = 0; k < temp % 10 % 5; k++)
				str = '|' + str;
			for (int j = 0; j < temp % 10 / 5; j++)
				str = '*' + str;
			if (temp % 10 == 0)
				str = '0' + str;
			temp = temp / 10;
			if (i < Integer.toString(num).length() - 1)
				str = ' ' + str;
		}
	}

	private String reverse(String givenString) {
		String s = "";
		for (int i = 0; i < givenString.length(); i++)
			s = givenString.charAt(i) + s;
		return s;
	}
}
