package mini1;

public class test {

	public static void main(String[] args) {

		// TallyNumber tn = new TallyNumber("||**|*|");
		// System.out.println(tn.getIntValue()); // expected 19
		//
		// TallyNumber tn1 = new TallyNumber("*|| 0 |||| *|||");
		// System.out.println(tn1.getIntValue());
		//
		// TallyNumber tn2 = new TallyNumber(17);
		// TallyNumber tn3 = new TallyNumber("||**|*");
		// tn2.add(tn3);
		// System.out.println(tn2.getIntValue());
		// System.out.println(tn2.getStringValue());

		TallyNumber t1 = new TallyNumber("*** ||");
		TallyNumber t2 = new TallyNumber("|||| *");
		t1.combine(t2);
		System.out.println(t1.getIntValue()); // 197
		System.out.println(t1.getStringValue()); // "***|||| ||*"

		TallyNumber t3 = new TallyNumber("| *");
		TallyNumber t4 = new TallyNumber("| ** 0 ||");
		t3.combine(t4);
		System.out.println(t3.getIntValue()); // 2017
		System.out.println(t3.getStringValue()); // "| ** |0 *||"

		TallyNumber a = new TallyNumber("");
		;
		System.out.println(a.getStringValue());
		System.out.println(a.getIntValue());

		String b = "";
		String c = "1";
		int d = 0;
		if (b == null || b.equals("")) {
			d = -1;
		}
		System.out.println(d);
	}

}
