package hw3;

import org.junit.*;
import static org.junit.Assert.*;
import api.*;
import java.util.ArrayList;
import java.io.FileNotFoundException;


public class UtilTests {

	private ArrayList<FlowGame> games;

	@Before
	public void makeGames() throws FileNotFoundException {
		games = Util.readFile("games.txt");
	}
	
	@Test
	public void numberGames(){
		assertEquals(10, games.size());
	}
	
	@Test
	public void onlyValidGamesConstructed(){
		for(FlowGame game : games){
			assertTrue(game.getHeight()>0&game.getWidth()>0);
			assertTrue(game.getAllFlows()!=null);
			assertFalse(game.isComplete());
		}
	}
	
	@Test
	public void numberFlows(){
		int[] numFlows = {1, 4, 5, 5, 4, 5, 7, 6, 8, 8};
		int i=0;
		for(FlowGame game : games){
			assertEquals(numFlows[i], game.getAllFlows().length);
			i++;
		}
	}
	
	@Test
	public void eachFlowIncomplete(){
		for(FlowGame game : games){
			for(Flow flow : game.getAllFlows()){
				assertFalse(flow.isComplete());
			}
		}
	}
	
	@Test
	public void eachGameIncomplete(){
		for(FlowGame game : games){
			assertFalse(game.isComplete());
		}
	}
	
	
}
