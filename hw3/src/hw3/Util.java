package hw3;

import java.awt.Color;

/**
 * @author Yijia Huang
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import api.Cell;
import api.Flow;

/**
 * Utility class with methods for creating Flows from string descriptors and
 * reading string descriptors from a file. A string descriptor is an array of
 * strings, all with the same length, in which an alphabetic character at a
 * given row and column represents a cell at that row and column. The color of
 * the cell is determined from the character by the Cell constructor. A
 * descriptor is invalid if not all strings are the same length or if there is
 * an alphabetic character that does not appear exactly twice.
 */
public class Util {
	/**
	 * Creates an array of Flow objects based on the string descriptor. If the
	 * descriptor is invalid, this method returns null.
	 * 
	 * @param descriptor
	 *            array of strings
	 * @return array of Flow objects determined by the descriptor
	 */
	public static Flow[] createFlowsFromStringArray(String[] descriptor) {
		// TODO
		int Stringlength = descriptor[0].length();
		ArrayList<Cell> endpoints = new ArrayList<Cell>();
		ArrayList<Flow> flows = new ArrayList<Flow>();
		for (int i = 0; i < descriptor.length; i++) {
			Scanner scan = new Scanner(descriptor[i]);
			if (!(Stringlength == descriptor[i].length())) {
				scan.close();
				return null;
			}
			for (int j = 0; j < Stringlength; j++)
				if (descriptor[i].charAt(j) == 'R' || descriptor[i].charAt(j) == 'G' || descriptor[i].charAt(j) == 'B'
						|| descriptor[i].charAt(j) == 'C' || descriptor[i].charAt(j) == 'Y'
						|| descriptor[i].charAt(j) == 'M' || descriptor[i].charAt(j) == 'O'
						|| descriptor[i].charAt(j) == 'P' || descriptor[i].charAt(j) == 'S'
						|| descriptor[i].charAt(j) == 'V' || descriptor[i].charAt(j) == 'F')
					endpoints.add(new Cell(i, j, descriptor[i].charAt(j)));
				else if (descriptor[i].charAt(j) == '-') {
				} else {
					scan.close();
					return null;
				}
			scan.close();
		}
		ArrayList<Color> color = new ArrayList<Color>();
		for (int i = 0; i < endpoints.size(); i++)
			for (int j = i + 1; j < endpoints.size(); j++)
				if (endpoints.get(i).colorMatches(endpoints.get(j).getColor())
						&& !color.contains(endpoints.get(i).getColor())) {
					color.add(endpoints.get(i).getColor());
					flows.add(new Flow(endpoints.get(i), endpoints.get(j)));
					endpoints.remove(j);
					endpoints.remove(i);
					i = -1;
					break;
				}
		if (!endpoints.isEmpty())
			return null;
		return flows.toArray(new Flow[0]);
	}

	/**
	 * Reads the given file and constructs a list of FlowGame objects, one for
	 * each descriptor in the file. Descriptors in the file are separated by
	 * some amount of whitespace, but the file need not end with whitespace and
	 * may have extra whitespace at the beginning. Invalid descriptors in the
	 * file are ignored, so the method may return an empty list.
	 * 
	 * @param filename
	 *            name of the file to read
	 * @return list of FlowGame objects created from the valid descriptors in
	 *         the file
	 * @throws FileNotFoundException
	 */
	public static ArrayList<FlowGame> readFile(String filename) throws FileNotFoundException {
		// TODO
		File file = new File(filename);
		ArrayList<FlowGame> flowgame = new ArrayList<FlowGame>();
		String str = new String();
		ArrayList<String> strlist = new ArrayList<String>();
		Scanner scan = new Scanner(file);
		while (scan.hasNextLine()) {
			str = scan.nextLine();
			if (!str.trim().isEmpty())
				strlist.add(str);
			if (!strlist.isEmpty() && str.trim().isEmpty()) {
				if (new FlowGame(strlist.toArray(new String[0])).getAllFlows() != null)
					flowgame.add(new FlowGame(strlist.toArray(new String[0])));
				strlist.clear();
			}
		}
		if (!strlist.isEmpty() && new FlowGame(strlist.toArray(new String[0])).getAllFlows() != null)
			flowgame.add(new FlowGame(strlist.toArray(new String[0])));
		scan.close();
		return flowgame;
	}

}
