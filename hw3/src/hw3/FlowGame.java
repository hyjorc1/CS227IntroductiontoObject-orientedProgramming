package hw3;

/**
 * @author Yijia Huang
 */

import api.Cell;
import api.Flow;

/**
 * Game state for a Flow Free game.
 */
public class FlowGame {
	/**
	 * All flows in the FlowGame
	 */
	private Flow[] flows;

	/**
	 * Width of the FlowGame
	 */
	private int width;

	/**
	 * Height of the FlowGame
	 */
	private int height;

	/**
	 * The "current" cell corresponds to the current mouse or pointer position
	 */
	private Cell current;

	/**
	 * Constructs a FlowGame to use the given array of Flows and the given width
	 * and height. Client is responsible for ensuring that all cells in the
	 * given flows have row and column values within the given width and height.
	 * 
	 * @param givenFlows
	 *            an array of Flow objects
	 * @param givenWidth
	 *            width to use for the game
	 * @param givenHeight
	 *            height to use for the game
	 */
	public FlowGame(Flow[] givenFlows, int givenWidth, int givenHeight) {
		// TODO
		if (givenFlows != null && givenWidth >= 0 && givenHeight >= 0) {
			flows = new Flow[givenFlows.length];
			for (int i = 0; i < givenFlows.length; i++)
				flows[i] = givenFlows[i];
			width = givenWidth;
			height = givenHeight;
			current = null;
		}
	}

	/**
	 * Constructs a FlowGame from the given descriptor.
	 * 
	 * @param descriptor
	 *            array of strings representing initial endpoint positions
	 */
	public FlowGame(String[] descriptor) {
		// TODO
		Flow[] temp = Util.createFlowsFromStringArray(descriptor);
		if (temp != null) {
			flows = new Flow[temp.length];
			for (int i = 0; i < temp.length; i++)
				flows[i] = temp[i];
			width = descriptor[0].length();
			height = descriptor.length;
			current = null;
		}
	}

	/**
	 * Returns the width for this game.
	 * 
	 * @return width for this game
	 */
	public int getWidth() {
		// TODO
		return width;
	}

	/**
	 * Returns the height for this game.
	 * 
	 * @return height for this game
	 */
	public int getHeight() {
		// TODO
		return height;
	}

	/**
	 * Returns the current cell for this game, possible null.
	 * 
	 * @return current cell for this game
	 */
	public Cell getCurrent() {
		// TODO
		return current;
	}

	/**
	 * Returns all flows for this game. Client should not modify the returned
	 * array or lists.
	 * 
	 * @return array of flows for this game
	 */
	public Flow[] getAllFlows() {
		// TODO
		return flows;
	}

	/**
	 * Returns the number of occupied cells in all flows (including endpoints).
	 * 
	 * @return occupied cells in this game
	 */
	public int getCount() {
		// TODO
		int count = 0;
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				if (isOccupied(i, j))
					count++;
		return count;
	}

	/**
	 * Returns true if all flows are complete and all cells are occupied.
	 * 
	 * @return true if all flows are complete and all cells are occupied
	 */
	public boolean isComplete() {
		// TODO
		boolean result = true;
		for (Flow e : flows)
			result &= e.isComplete();
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
				result &= isOccupied(i, j);
		return result;
	}

	/**
	 * Attempts to set the "current" cell to be an existing cell at the given
	 * row and column. When using a GUI, this method is typically invoked when
	 * the mouse is pressed.
	 * <ul>
	 * <li>Any endpoint can be selected as the current cell. Selecting an
	 * endpoint clears the flow associated with that endpoint.
	 * <li>A non-endpoint cell can be selected as the current cell only if it is
	 * the last cell in a flow.
	 * </ul>
	 * If neither of the above conditions is met, this method does nothing.
	 * 
	 * @param row
	 *            given row
	 * @param col
	 *            given column
	 */
	public void startFlow(int row, int col) {
		// TODO
		for (int i = 0; i < flows.length; i++) {
			if (flows[i].getEndpoint(0).positionMatches(row, col)
					|| flows[i].getEndpoint(1).positionMatches(row, col)) {
				current = new Cell(row, col, flows[i].getColor());
				flows[i].clear();
				flows[i].add(current);
			} else if (!flows[i].getCells().isEmpty()
					&& flows[i].getCells().get(flows[i].getCells().size() - 1).positionMatches(row, col)) {
				current = new Cell(row, col, flows[i].getColor());
				flows[i].add(current);
			}

		}
	}

	/**
	 * Clears the "current" cell. That is, directly after invoking this method,
	 * <code>getCurrent</code> returns null. When using a GUI, this method is
	 * typically invoked when the mouse is released.
	 */
	public void endFlow() {
		// TODO
		current = null;
	}

	/**
	 * Attempts to add a new cell to the flow containing the current cell. When
	 * using a GUI, this method is typically invoked when the mouse is dragged.
	 * In order to add a cell, the following conditions must be satisfied:
	 * <ol>
	 * <li>The current cell is non-null
	 * <li>The given position is horizontally or vertically adjacent to the
	 * current cell
	 * <li>The given position either is not occupied OR it is occupied by an
	 * endpoint for the flow that is not already in the flow
	 * </ul>
	 * If the three conditions are met, a new cell with the given row/column and
	 * correct color is added to the current flow. If the added cell is not an
	 * endpoint, it becomes the current cell. If the added cell is the second
	 * endpoint, the current cell becomes null.
	 * 
	 * @param row
	 *            given row for the new cell
	 * @param col
	 *            given column for the new cell
	 */
	public void addCell(int row, int col) {
		// TODO
		if (current != null && row <= height && col <= width && row >= 0 && col >= 0)
			for (Flow e : flows) {
				if (e.getCells().contains(current) && !e.isComplete()
						&& (((current.getRow() == row - 1 || current.getRow() == row + 1) && current.getCol() == col)
								|| ((current.getCol() == col - 1 || current.getCol() == col + 1)
										&& current.getRow() == row))
						&& (!isOccupied(row, col) || e.getEndpoint(0).positionMatches(row, col)
								|| e.getEndpoint(1).positionMatches(row, col))
						&& (!e.getCells().contains(new Cell(row, col, current.getColor())))) {
					if (!e.getEndpoint(0).positionMatches(row, col) && !e.getEndpoint(1).positionMatches(row, col))
						current = new Cell(row, col, current.getColor());
					e.add(new Cell(row, col, current.getColor()));
					if (e.isComplete())
						current = null;
				}
			}
	}

	/**
	 * Returns true if the given position is occupied by a cell in a flow in
	 * this game (possibly an endpoint).
	 * 
	 * @param row
	 *            given row
	 * @param col
	 *            given column
	 * @return true if any cell in this game has the given row and column, false
	 *         otherwise
	 */
	public boolean isOccupied(int row, int col) {
		// TODO
		for (Flow f : flows) {
			if (f.getEndpoint(0).positionMatches(row, col) || f.getEndpoint(1).positionMatches(row, col))
				return true;
			for (int i = 0; i < f.getCells().size(); i++)
				if (f.getCells().get(i).positionMatches(row, col))
					return true;
		}
		return false;
	}

}
