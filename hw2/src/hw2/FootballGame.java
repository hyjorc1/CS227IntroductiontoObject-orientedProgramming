package hw2;

/**
 * This class encapsulates the logic and state for a simplified game of American
 * football.
 * 
 * @author Yijia Huang
 */
public class FootballGame {
	/**
	 * Number of points awarded for a touchdown.
	 */
	public static final int TOUCHDOWN_POINTS = 6;

	/**
	 * Number of points awarded for a successful extra point attempt after a
	 * touchdown.
	 */
	public static final int EXTRA_POINTS = 1;

	/**
	 * Number of points awarded for a field goal.
	 */
	public static final int FIELD_GOAL_POINTS = 3;

	/**
	 * Total length of the field from goal line to goal line, in yards.
	 */
	public static final int FIELD_LENGTH = 100;

	/**
	 * Initial position of the offensive team after receiving a kickoff.
	 */
	public static final int STARTING_POSITION = 70;

	/**
	 * Yards required to get a first down.
	 */
	public static final int YARDS_FOR_FIRST_DOWN = 10;

	/**
	 * which team (0 or 1) is currently playing offense
	 */
	private int OffenseTeam;

	/**
	 * The current down.
	 */
	private int CurrentDown;

	/**
	 * yards needed to advance to a first down
	 */
	private int YardsToFirstDown;

	/**
	 * Total yards from the opposite goal line to the location of ball.
	 */
	private int CurrentPosition;

	/**
	 * Points for team 0.
	 */
	private int TeamZeroScore;

	/**
	 * Points for team 1.
	 */
	private int TeamOneScore;

	/**
	 * Status of TouchDown
	 */
	private boolean TouchDown;

	/**
	 * Constructs a new game. Initially team 0 is offense, the ball is
	 * STARTING_POSITION yards from the defense's goal line, and it is the first
	 * down.
	 */
	public FootballGame() {
		OffenseTeam = 0;
		CurrentPosition = STARTING_POSITION;
		YardsToFirstDown = YARDS_FOR_FIRST_DOWN;
		TeamZeroScore = 0;
		TeamOneScore = 0;
		CurrentDown = 1;
		TouchDown = false;
	}

	/**
	 * Returns the location of the ball as the number of yards to the opposing
	 * team's goal line.
	 * 
	 * @return number of yards from the ball to the defense's goal line
	 */
	public int getYardsToGoalLine() {
		return CurrentPosition;
	}

	/**
	 * Returns the number of yards the offense must advance the ball to get a
	 * first down.
	 * 
	 * @return number of yards to get a first down
	 */
	public int getYardsToFirstDown() {
		return YardsToFirstDown;
	}

	/**
	 * Returns the points for the indicated team.
	 * 
	 * @param whichTeam
	 *            team index 0 or 1
	 * @return score for team 0 if the given argument is 0, score for team 1
	 *         otherwise
	 */
	public int getScore(int whichTeam) {
		if (whichTeam == 0) {
			return TeamZeroScore;
		} else {
			return TeamOneScore;
		}
	}

	/**
	 * Returns the current down. Possible values are 1 through 4.
	 * 
	 * @return the current down as a number 1 through 4
	 */
	public int getDown() {
		return CurrentDown;
	}

	/**
	 * Returns the index for the team currently playing offense.
	 * 
	 * @return index of the team playing offense (0 or 1)
	 */
	public int getOffense() {
		return OffenseTeam;
	}

	/**
	 * Records the result of advancing the ball the given number of yards,
	 * possibly resulting in a first down, a touchdown, or a turnover.
	 * 
	 * @param yards
	 *            number of yards (possibly negative) the ball is advanced
	 */
	public void runOrPass(int yards) {
		if (TouchDown == false) {
			YardsToFirstDown -= yards;
			CurrentPosition = Math.min(CurrentPosition - yards, 100);
			if (CurrentPosition <= 0) {
				TeamZeroScore += TOUCHDOWN_POINTS * (1 - OffenseTeam);
				TeamOneScore += TOUCHDOWN_POINTS * OffenseTeam;
				TouchDown = true;
			} else if (YardsToFirstDown <= 0) {
				CurrentDown = 1;
				YardsToFirstDown = YARDS_FOR_FIRST_DOWN;
			} else if (CurrentDown == 4) {
				OffenseTeam = 1 - OffenseTeam;
				TouchDown = false;
				CurrentDown = 1;
				YardsToFirstDown = YARDS_FOR_FIRST_DOWN;
				CurrentPosition = FIELD_LENGTH - CurrentPosition;
			} else {
				CurrentDown++;
			}
		}
	}

	/**
	 * Records the result of an extra point attempt, adding EXTRA_POINTS points
	 * if the attempt was successful. Whether or not the attempt is successful,
	 * the defense gets the ball and starts with a first down, STARTING_POSITION
	 * yards from the goal line.
	 * 
	 * @param success
	 *            true if the extra point was successful, false otherwise
	 */
	public void extraPoint(boolean success) {
		if (success) {
			TeamZeroScore += EXTRA_POINTS * (1 - OffenseTeam);
			TeamOneScore += EXTRA_POINTS * OffenseTeam;
		}
		OffenseTeam = 1 - OffenseTeam;
		TouchDown = false;
		CurrentDown = 1;
		YardsToFirstDown = YARDS_FOR_FIRST_DOWN;
		CurrentPosition = STARTING_POSITION;
	}

	/**
	 * Records the result of a field goal attempt, adding FIELD_GOAL_POINTS
	 * points if the field goal was successful. If the attempt is successful,
	 * the defense gets the ball and starts with a first down, STARTING_POSITION
	 * yards from the goal line. If the attempt is unsuccessful, the defense
	 * gets the ball at its current location.
	 * 
	 * @param success
	 *            true if the attempt was successful, false otherwise
	 */
	public void fieldGoal(boolean success) {
		if (success) {
			TeamZeroScore += FIELD_GOAL_POINTS * (1 - OffenseTeam);
			TeamOneScore += FIELD_GOAL_POINTS * OffenseTeam;
			CurrentPosition = STARTING_POSITION;
		} else {
			CurrentPosition = FIELD_LENGTH - CurrentPosition;
		}
		OffenseTeam = 1 - OffenseTeam;
		TouchDown = false;
		CurrentDown = 1;
		YardsToFirstDown = YARDS_FOR_FIRST_DOWN;
	}

	/**
	 * Records the result of a punt. The defense gets the ball with a first down
	 * after it has advanced the given number of yards; however, if the ball
	 * would have advanced past the defense's goal line, the defense starts with
	 * the ball at location FIELD_LENGTH (i.e. it can't be behind their own goal
	 * line). The given number of yards should not be negative.
	 * 
	 * @param yards
	 *            number of yards the ball is advanced by the punt (not less
	 *            than zero)
	 */
	public void punt(int yards) {
		CurrentPosition -= yards;
		OffenseTeam = 1 - OffenseTeam;
		TouchDown = false;
		CurrentDown = 1;
		YardsToFirstDown = YARDS_FOR_FIRST_DOWN;
		if (CurrentPosition <= 0) {
			CurrentPosition = FIELD_LENGTH;
		} else {
			CurrentPosition = FIELD_LENGTH - CurrentPosition;
		}

	}

}
