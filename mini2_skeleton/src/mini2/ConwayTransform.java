package mini2;

import api.ITransform;

/**
 * Transformation implementing Conway's Game of Life. All cells have value 0 or
 * 1. The new value is based on the center cell along with the sum S of its
 * eight neighbors, according to the rules:
 * <ul>
 * <li>if S is less than 2 the result is 0
 * <li>if S is greater than 3 the result is 0
 * <li>if the center cell is 1 and S is 2 or 3, the result is 1
 * <li>if the center cell is 0 and S is exactly 3, the result is 1
 * </ul>
 * See http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
 *
 * <p>
 * The radius is 1 and the <code>isWrapped()</code> method always returns true.
 */
public class ConwayTransform implements ITransform {

	@Override
	public int apply(int[][] elements) {
		// TODO
		if (getRadius() * 2 + 1 != elements.length || getRadius() * 2 + 1 != elements[0].length)
			throw new IllegalArgumentException();
		int s = 0;
		for (int i = 0; i < elements.length; i++)
			for (int j = 0; j < elements[0].length; j++)
				if (i != getRadius() || j != getRadius())
					s += elements[i][j];
		System.out.println(s);
		if (s < 2 || s > 3)
			return 0;
		if (elements[getRadius()][getRadius()] == 1 && (s == 2 || s == 3))
			return 1;
		if (elements[getRadius()][getRadius()] == 0 && s == 3)
			return 1;
		return elements[getRadius()][getRadius()];
	}

	@Override
	public int getRadius() {
		return 1;
	}

	@Override
	public boolean isWrapped() {
		return true;
	}

}
