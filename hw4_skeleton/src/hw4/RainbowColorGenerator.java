package hw4;

import java.awt.Color;
import java.util.Random;

import color.ColorGenerator;
import main.Config;

/**
 * 
 * @author Yijia Huang
 * 
 *         Randomly creates one of 6 dark rainbow colors: Blue, Green, Yellow,
 *         Orange, Red, Purple, Blue.
 *
 */
public class RainbowColorGenerator implements ColorGenerator {
	/**
	 * The number generator for creating colors.
	 */
	private Random r = Config.RANDOM;

	@Override
	public Color createColor() {
		// TODO Auto-generated method stub
		int next = r.nextInt(6);
		switch (next) {
		case 0:
			return new Color(0.25f, 0.0f, 0.0f);
		case 1:
			return new Color(0.25f, 0.125f, 0.0f);
		case 2:
			return new Color(0.25f, 0.25f, 0.0f);
		case 3:
			return new Color(0.0f, 0.25f, 0.0f);
		case 4:
			return new Color(0.0f, 0.0f, 0.25f);
		default:
			return new Color(0.25f, 0.0f, 0.25f);
		}
	}

}
