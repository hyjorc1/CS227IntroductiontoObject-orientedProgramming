package hw4;

import java.awt.Color;

import graph.Cell;
import main.Config;
import main.Main;
import state.SnakeSegment;
import state.State;

public class test {

	public static void main(String[] args) {
		// wall
		// Wall w = new Wall();
		// System.out.println(w.getColor()); //Should print
		// 'java.awt.Color[r=255,g=255,b=255]'
		// System.out.println(w.toChar()); //Should print '#'
		// System.out.println(w.isPassable()); //Should print false

		// food
		// Food f = new Food();
		// System.out.println(f.getColor().getAlpha());
		// System.out.println(f.toChar()); //Should be 'F'
		// System.out.println(f.isPassable()); //Should be true
		//
		// Cell c = new Cell(null, null); // color and polygon don't matter,
		// since we don't plan to draw it
		// f.handle(c);
		// System.out.println(f.getColor().getAlpha()); //Alpha should change as
		// you call handle
		// f.handle(c);
		// System.out.println(f.getColor().getAlpha()); //Alpha should change as
		// you call handle

		// DungeonessCrab
		// Create a cell whose state is DungeonessCrab and
		// give it an open neighbor. Note the color and
		// polygon won't be used, so we can leave them null
		// Cell c = new Cell(null, null);
		// State myCrab = new DungeonessCrab();
		// State food = new Food();
		// System.out.println(myCrab.getColor());
		// System.out.println(food.getColor());
		// c.setState(myCrab);
		// Cell c2 = new Cell(null, null);
		// Cell[] neighbors = { c2 };
		// c.setNeighbors(neighbors);
		//
		// // check initial values
		// System.out.println(c.getState() == myCrab); // should be true
		// System.out.println(c2.getState()); // should be null
		//
		// // do updates (calls handle())
		// for (int i = 0; i < Config.MAX_FOOD_TIMER; ++i) {
		// c.update();
		// }
		//
		// // crab state should have moved to neighbor
		// System.out.println(c.getState() == myCrab); // should be null
		// System.out.println(c2.getState() == myCrab); // should be trueF

		// snakehead
		// // simple test grid, three cells in a row
		// Cell c0 = new Cell(null, null);
		// Cell c1 = new Cell(null, null);
		// Cell c2 = new Cell(null, null);
		// c2.setNeighbors(new Cell[]{});
		// c1.setNeighbors(new Cell[]{c2});
		// c0.setNeighbors(new Cell[]{c1});
		//
		// // make a snake head
		// SnakeHead head = new SnakeHead();
		// c0.setState(head);
		//
		// // do enough updates that it should "move"
		// for (int i = 0; i < Config.MAX_SNAKE_TIMER; ++i)
		// {
		// c0.update();
		// c1.update();
		// c2.update();
		// }
		//
		// // now c0 should be a segment, and the head should be at c1
		// System.out.println(c0.getState().getClass()); // should be
		// SnakeSegment
		// System.out.println(c1.getState() == head); // should be true
		// System.out.println(c2.getState()); // should be null
		// System.out.println(head.getLength()); // expected 4
		//
		// // ok, now try some food
		// c2.setState(new Food());
		//
		// // do enough updates that it should "move" again
		// for (int i = 0; i < Config.MAX_SNAKE_TIMER; ++i)
		// {
		// c0.update();
		// c1.update();
		// c2.update();
		// }
		//
		// System.out.println(c0.getState().getClass()); // should be
		// SnakeSegment
		// System.out.println(c1.getState().getClass()); // should be
		// SnakeSegment
		// System.out.println(c2.getState() == head); // should be true
		// System.out.println(head.getLength()); // expected 5

		// RainbowColorGenerator
		// int redcount = 0;
		// int orangecount = 0;
		// int yellowcount = 0;
		// int greencount = 0;
		// int bluecount = 0;
		// int purplecount = 0;
		//
		// for (int i = 0; i < 1000; i++) {
		// RainbowColorGenerator test = new RainbowColorGenerator();
		// if (test.createColor().equals(new Color(0.25f, 0.0f, 0.0f))) {
		// redcount++;
		// }
		// if (test.createColor().equals(new Color(0.25f, 0.125f, 0.0f))) {
		// orangecount++;
		// }
		// if (test.createColor().equals(new Color(0.0f, 0.0f, 0.25f))) {
		// bluecount++;
		// }
		// if (test.createColor().equals(new Color(0.25f, 0.0f, 0.25f))) {
		// purplecount++;
		// }
		// if (test.createColor().equals(new Color(0.0f, 0.25f, 0.0f))) {
		// greencount++;
		// }
		// if (test.createColor().equals(new Color(0.25f, 0.25f, 0.0f))) {
		// yellowcount++;
		// }
		// }
		// System.out.println("Red:" + redcount);
		// System.out.println("Orange:" + orangecount);
		// System.out.println("Blue:" + bluecount);
		// System.out.println("Purple:" + purplecount);
		// System.out.println("Green:" + greencount);
		// System.out.println("Yellow:" + yellowcount);
		// }
		//
		// // EACH COLOR SHOULD BE ROUGHLY 1/6TH OF 1000

		//recal1
		// Cell c0 = new Cell(null, null);
		// Cell c1 = new Cell(null, null);
		// Cell c2 = new Cell(null, null);
		// c2.setNeighbors(new Cell[]{});
		// c1.setNeighbors(new Cell[]{c2});
		// c0.setNeighbors(new Cell[]{c1});
		//
		// c0.recalculateMouseDistances();
		//
		// System.out.println(c0.getMouseDistance()); // expected 20
		// System.out.println(c1.getMouseDistance()); // expected 19
		// System.out.println(c2.getMouseDistance()); // expected 18
		//
	  //recal2
		 Cell c1 = new Cell(null, null);
		 Cell c2 = new Cell(null, null);
		 Cell c3 = new Cell(null, null);
		 Cell c4 = new Cell(null, null);
		 Cell c5 = new Cell(null, null);
		 Cell c6 = new Cell(null, null);
		 Cell c7 = new Cell(null, null);
		 Cell c8 = new Cell(null, null);
		 Cell c9 = new Cell(null, null);
		 Cell c10 = new Cell(null, null);
		 Cell c11 = new Cell(null, null);
		 Cell c12 = new Cell(null, null);
		 Cell c13 = new Cell(null, null);
		 Cell c14 = new Cell(null, null);
		
		 c1.setNeighbors(new Cell[]{c6, c2});
		 c2.setNeighbors(new Cell[]{c1, c6, c5, c3});
		 c3.setNeighbors(new Cell[]{c2,c5,c4});
		 c4.setNeighbors(new Cell[]{c3,c5,c8});
		 c5.setNeighbors(new Cell[]{c2,c6,c7,c8,c4,c3});
		 c6.setNeighbors(new Cell[]{c1, c2, c5, c7, c14});
		 c7.setNeighbors(new Cell[]{c14, c13, c10, c8, c5, c6});
		 c8.setNeighbors(new Cell[]{c4, c5, c7, c10, c9});
		 c9.setNeighbors(new Cell[]{c8,c10,c11});
		 c10.setNeighbors(new Cell[]{c7,c8,c9,c11,c12,c13});
		 c11.setNeighbors(new Cell[]{c9,c10,c12});
		 c12.setNeighbors(new Cell[]{c13,c10,c11});
		 c13.setNeighbors(new Cell[]{c14,c7,c10,c12});
		 c14.setNeighbors(new Cell[]{c6,c7,c13});
		
//		 c1.recalculateMouseDistances();
//		
//		 // Comments that follow are the expected values
//		
//		 System.out.println(c1.getMouseDistance()); // 20
//		 System.out.println(c2.getMouseDistance()); // 19
//		 System.out.println(c3.getMouseDistance()); // 18
//		 System.out.println(c4.getMouseDistance()); // 17
//		 System.out.println(c5.getMouseDistance()); // 18
//		 System.out.println(c6.getMouseDistance()); // 19
//		 System.out.println(c7.getMouseDistance()); // 18
//		 System.out.println(c8.getMouseDistance()); // 17
//		 System.out.println(c9.getMouseDistance()); // 16
//		 System.out.println(c10.getMouseDistance()); // 17
//		 System.out.println(c11.getMouseDistance()); // 16
//		 System.out.println(c12.getMouseDistance()); // 16
//		 System.out.println(c13.getMouseDistance()); // 17
//		 System.out.println(c14.getMouseDistance()); // 18
		 
		 System.out.println("-------------");
		
		//3
		SnakeHead s1 = new SnakeHead();
		SnakeSegment s2 = new SnakeSegment(s1);
		Wall w1 = new Wall();
	 	Food f1 = new Food();
	 	DungeonessCrab d1 = new DungeonessCrab();
	 	c1.setState(d1);
		c4.setState(s1);
		c3.setState(s2);
		c7.setState(w1);
		c9.setState(w1);
		c8.setState(f1);
		
		c1.recalculateMouseDistances();
		
		System.out.println(c1.getMouseDistance());	// 20
		System.out.println(c2.getMouseDistance());	// 19
		System.out.println(c3.getMouseDistance());	// 0
		System.out.println(c4.getMouseDistance());	// 0
		System.out.println(c5.getMouseDistance());	// 18
		System.out.println(c6.getMouseDistance());	// 19
		System.out.println(c7.getMouseDistance());	// 0
		System.out.println(c8.getMouseDistance());	// 17
		System.out.println(c9.getMouseDistance());	// 0
		System.out.println(c10.getMouseDistance());	// 16
		System.out.println(c11.getMouseDistance());	// 15
		System.out.println(c12.getMouseDistance());	// 16
		System.out.println(c13.getMouseDistance());	// 17
		System.out.println(c14.getMouseDistance());	// 18
		
		System.out.println("--------");
	}
}
