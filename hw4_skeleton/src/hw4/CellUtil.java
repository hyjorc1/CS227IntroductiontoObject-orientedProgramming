package hw4;

import java.util.ArrayList;

import graph.Cell;

/**
 * 
 * @author Yijia Huang
 *
 *         Class CellUtil is used to update the GraphMap when the method
 *         recalculteMouseDistance is called.
 */
public class CellUtil {
	/**
	 * Sets the mouse distance for the given cell and recursively sets the mouse
	 * distance for all neighboring cells that a) do not already have a larger
	 * mouse distance and b) are open or passable. Neighboring cells satisfying
	 * these conditions are set to <code>distance - 1</code>. If the given
	 * <code>distance</code> is less than or equal to zero, this method does
	 * nothing.
	 * 
	 * @param cell
	 *            the cell whose distance is to be set
	 * @param distance
	 *            the distance value to be set in the given cell
	 */
	public static void calculateMouseDistance(Cell cell, int distance) {
		// TODO
		if (distance <= 0)
			return;
		if (distance == 20)
			cell.setMouseDistance(distance);
		ArrayList<Cell> cells = new ArrayList<Cell>();
		for (Cell e : cell.getNeighbors())
			if (e.getMouseDistance() == 0)
				if (e.getState() == null || e.getState().isPassable()) {
					for (Cell E : e.getNeighbors())
						if (E.getMouseDistance() > distance) {
							distance = E.getMouseDistance() - 1;
							e.setMouseDistance(distance);
						}
					cells.add(e);
				}
		for (int i = 0; i < cells.size(); i++)
			calculateMouseDistance(cells.get(i), distance - 1);
	}

}
