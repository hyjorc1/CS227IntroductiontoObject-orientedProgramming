package hw4;

import graph.Cell;
import main.Config;
import state.State;

/**
 * 
 * @author Yijia Huang
 * 
 *         Randomly moves around according to the cycle of food. A "D" in a map
 *         file.
 */
public class DungeonessCrab extends Food implements State {

	private int timer = 0;

	@Override
	public void handle(Cell cell) {
		// TODO Auto-generated method stub
		timer++;
		if (timer == Config.MAX_FOOD_TIMER) {
			timer = 0;
			cell.moveState(cell.getRandomOpen());
		}
	}

	@Override
	public char toChar() {
		// TODO Auto-generated method stub
		return 'D';
	}

}
