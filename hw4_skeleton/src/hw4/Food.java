package hw4;

import java.awt.Color;

import graph.Cell;
import main.Config;
import state.State;

/**
 * 
 * @author Yijia Huang
 * 
 *         A flashing passable state that uses the default food variables. An
 *         "F" in a map file.
 *
 */
public class Food implements State {

	/**
	 * The number for frames this state has persisted.
	 */
	private int timer = 0;

	@Override
	public void handle(Cell cell) {
		// TODO Auto-generated method stub
		timer++;
		if (timer == Config.MAX_FOOD_TIMER)
			timer = 0;
	}

	@Override
	public Color getColor() {
		// TODO Auto-generated method stub
		return Config.FOOD_COLORS[timer];
	}

	@Override
	public boolean isPassable() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public char toChar() {
		// TODO Auto-generated method stub
		return 'F';
	}

}
