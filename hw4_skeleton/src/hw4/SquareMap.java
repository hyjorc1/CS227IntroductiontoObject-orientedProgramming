package hw4;

import java.awt.Point;
import java.awt.Polygon;
import java.util.ArrayList;

import graph.Cell;
import graph.GraphMap;

/**
 * 
 * @author Yijia Huang
 * 
 *         A square lattice where each cell has 4 neighbors set up in a checker
 *         board pattern.
 *
 */
public class SquareMap extends GraphMap {

	@Override
	public int getPixelWidth() {
		// TODO Auto-generated method stub
		return (getCells()[0].length + 1) * getDistance();
	}

	@Override
	public int getPixelHeight() {
		// TODO Auto-generated method stub
		return (getCells().length + 1) * getDistance();
	}

	@Override
	public Cell[] createNeighbors(int col, int row) {
		// TODO Auto-generated method stub
		Cell[][] map = getCells();
		int[][] relative = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };
		ArrayList<Cell> cells = new ArrayList<Cell>();
		for (int i = 0; i < relative.length; i++)
			if (row + relative[i][0] >= 0 && row + relative[i][0] < map.length)
				if (col + relative[i][1] >= 0 && col + relative[i][1] < map[0].length)
					cells.add(map[row + relative[i][0]][col + relative[i][1]]);
		return cells.toArray(new Cell[0]);
	}

	@Override
	protected Point selectClosestIndex(int x, int y) {
		// TODO Auto-generated method stub
		return new Point((x - getDistance() / 2) / getDistance(), (y - getDistance() / 2) / getDistance());
	}

	@Override
	public Polygon createPolygon(int col, int row) {
		// TODO Auto-generated method stub
		int[] xCoor = { col * getDistance() + getDistance() / 2, (col + 1) * getDistance() + getDistance() / 2,
				(col + 1) * getDistance() + getDistance() / 2, col * getDistance() + getDistance() / 2 };
		int[] yCoor = { row * getDistance() + getDistance() / 2, row * getDistance() + getDistance() / 2,
				(row + 1) * getDistance() + getDistance() / 2, (row + 1) * getDistance() + getDistance() / 2 };
		return new Polygon(xCoor, yCoor, 4);
	}

}
