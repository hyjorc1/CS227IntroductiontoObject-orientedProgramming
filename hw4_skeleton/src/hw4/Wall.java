package hw4;

import java.awt.Color;

import graph.Cell;
import state.State;

/**
 * 
 * @author Yijia Huang
 *
 *         An impassable state with the color White. Doesn't do anything. A "#"
 *         in the map file.
 */
public class Wall implements State {

	@Override
	public void handle(Cell cell) {
		// TODO Auto-generated method stub
	}

	@Override
	public Color getColor() {
		// TODO Auto-generated method stub
		return Color.white;
	}

	@Override
	public boolean isPassable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public char toChar() {
		// TODO Auto-generated method stub
		return '#';
	}
}
